import java.sql.*;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.util.StringJoiner;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CreationHelper;

public class ExcelUtil {
    public static void main(String[] args) throws ClassNotFoundException, IOException, SQLException {
        Class.forName("com.mysql.jdbc.Driver");
        Connection conn = DriverManager.getConnection("jdbc:mysql://localhost/test", "temp_user", "pass1234");
        Statement stmt = conn.createStatement();
        ResultSet rs = stmt.executeQuery("select * from information_schema.TABLES limit 10");
        
        writeExcel(rs, "t1.xlsx");
        rs = stmt.executeQuery("select * from information_schema.TABLES limit 10");
        writeCSV(rs, "t1.csv", new Character('\u0001').toString());

        conn.close();
    }

    public static void writeCSV(ResultSet rs, String filename, String delimiter) 
            throws SQLException, IOException {
        BufferedWriter bufWriter = new BufferedWriter(
                                                new FileWriter(filename));

        ResultSetMetaData metaData = rs.getMetaData();
        int numColumns = metaData.getColumnCount();
        StringJoiner joiner = null;

        // Write header
        joiner = new StringJoiner(delimiter);
        for(int i=1; i<=numColumns; ++i) {
            String colName = metaData.getColumnName(i);
            joiner.add(colName);
        }
        bufWriter.write(joiner.toString());
        bufWriter.newLine();

        // Write values
        while(rs.next()) {
            joiner = new StringJoiner(delimiter);
            for(int i=1; i<=numColumns; ++i) {
                String colName = rs.getString(i);
                joiner.add(colName);
            }
            bufWriter.write(joiner.toString());
            bufWriter.newLine();
        }

        bufWriter.close();
    }

    public static void writeExcel(ResultSet rs, String filename) throws IOException, SQLException {
        Workbook wb = new XSSFWorkbook();
        Sheet sheet = wb.createSheet("Sheet1");

        CreationHelper createHelper = wb.getCreationHelper();
        CellStyle dateStyle = wb.createCellStyle();
        dateStyle.setDataFormat(createHelper.createDataFormat().getFormat("yyyy/mm/dd h:mm:ss"));

        // Table column details
        ResultSetMetaData metaData = rs.getMetaData();
        int numColumns = metaData.getColumnCount();
        String columnTypes[] = new String[numColumns];
        
        Row headerRow = sheet.createRow(0);
        for(int i=1; i<=numColumns; ++i) {
            headerRow.createCell(i-1).setCellValue(metaData.getColumnName(i));
            columnTypes[i-1] = metaData.getColumnTypeName(i);
        }


        // Table values
        int rowCount = 0;
        while(rs.next()) {
            ++rowCount;
            Row row = sheet.createRow(rowCount);
            for(int i=1; i<=numColumns; ++i)
                createCell(row, rs, i, columnTypes[i-1], dateStyle);
        }

        FileOutputStream fos = new FileOutputStream(filename);
        wb.write(fos);
    }

    private static Cell createCell(Row row, ResultSet rs, int i, String columnType, CellStyle dateStyle) throws SQLException {
        Cell cell = row.createCell(i-1);
        switch(columnType.toUpperCase()) {
            case "CHAR":
            case "VARCHAR":
            case "LONGVARCHAR":
            case "STRING":
                cell.setCellType(CellType.STRING);
                cell.setCellValue(rs.getString(i));
                break;

            case "DATETIME":  // effects of timezone may cause problems
            case "DATE":
            case "TIME":
            case "TIMESTAMP":
                cell.setCellStyle(dateStyle);
                cell.setCellValue(rs.getTimestamp(i));
                break;

            case "NUMERIC":
            case "DECIMAL":
            case "REAL":
            case "FLOAT":
            case "DOUBLE":
                cell.setCellType(CellType.NUMERIC);
                cell.setCellValue(rs.getDouble(i));
                break;

            case "TINYINT":
            case "SMALLINT":
            case "INTEGER":
            case "BIGINT":
            case "BIGINT UNSIGNED":
            case "INT":
                cell.setCellType(CellType.NUMERIC);
                cell.setCellValue(rs.getLong(i));
                break;

            case "BIT":
            case "BOOLEAN":
                cell.setCellType(CellType.BOOLEAN);
                cell.setCellValue(rs.getBoolean(i));
                break;

            default:
                System.out.println(rs.getString(i));
                throw new SQLException("no mapping available for \""+rs.getString(i)+"\" of type "+ columnType);
        }
        return cell;
    }

}