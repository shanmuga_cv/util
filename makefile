classpath=lib/mysql-connector-java-5.1.26-bin.jar:lib/poi-3.16/poi-excelant-3.16.jar:lib/poi-3.16/lib/commons-codec-1.10.jar:lib/poi-3.16/lib/log4j-1.2.17.jar:lib/poi-3.16/lib/junit-4.12.jar:lib/poi-3.16/lib/commons-collections4-4.1.jar:lib/poi-3.16/lib/commons-logging-1.2.jar:lib/poi-3.16/poi-ooxml-schemas-3.16.jar:lib/poi-3.16/ooxml-lib/xmlbeans-2.6.0.jar:lib/poi-3.16/ooxml-lib/curvesapi-1.04.jar:lib/poi-3.16/poi-examples-3.16.jar:lib/poi-3.16/poi-3.16.jar:lib/poi-3.16/poi-ooxml-3.16.jar:lib/poi-3.16/poi-scratchpad-3.16.jar:bin

run: compile
	java -cp ${classpath} ExcelUtil

compile: ExcelUtil.java clean
	javac -cp ${classpath} -d bin ExcelUtil.java

clean:
	find . -wholename 'bin/*.class' -delete -o -name '*.xlsx' -delete -o -name '*.csv' -delete